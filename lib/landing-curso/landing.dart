import 'package:flutter/material.dart';

Widget landing() {
  Widget title = Row(children: <Widget>[
    Container(
      decoration: _debugDecoration(),
      margin: EdgeInsets.only(top: 270, left: 20, right: 20),
      child: Text(
        'Duwili Ella',
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
        textAlign: TextAlign.left,
      ),
    )
  ]);

  Widget star = Container(
    decoration: _debugDecoration(),
    margin: EdgeInsets.only(top: 271, right: 3),
    child: Icon(
      Icons.star,
      color: Color(0xFFf2c611),
    ),
  );
  Widget stars = Row(
    children: <Widget>[star, star, star, star, star],
  );

  Widget titleStars = Row(
    children: <Widget>[title, stars],
  );

  String articleDescription =
      '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquam ipsum lobortis, cursus turpis eleifend, ultricies magna.
      Phasellus vel lorem quam. Donec et risus tristique est consectetur vehicula. Aliquam euismod sit amet mauris sed euismod. 
      
      Fusce et tellus aliquet, pulvinar lacus et, placerat nunc. Sed sed blandit odio. Nulla commodo lorem arcu, sed viverra diam imperdiet ultricies.
      Pellentesque molestie condimentum vulputate. Mauris rhoncus nec est in lacinia. Cras non neque elit.
      Cras luctus ex nec bibendum consectetur.''';

  Widget articleBody = Container(
      decoration: _debugDecoration(),
      margin: EdgeInsets.only(top: 20, left: 20, right: 30),
      child: Text(articleDescription));

  final navigateButtom =
      FloatingActionButton.extended(onPressed: () {}, label: Text('Navigate'));

  Widget article = Column(
    children: <Widget>[titleStars, articleBody],
  );

  Widget generalWidget = Scaffold(
    appBar: AppBar(
      title: Text('Hola Mundo Feliz'),
      centerTitle: true,
    ),
    body: article,
  );
  return generalWidget;
}

dynamic _debugDecoration() {
  return BoxDecoration(
      color: Colors.red.withOpacity(0.3),
      border: Border.all(color: Colors.red.withOpacity(1), width: 1));
}
