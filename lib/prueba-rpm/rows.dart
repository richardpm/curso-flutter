import 'package:flutter/material.dart';

dynamic Rows() {
  var estructura = Scaffold(
      appBar:
          AppBar(title: Text("Prueba de Rows y columns"), centerTitle: true),
      body: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(5),
              decoration: myCustomDecoration(Colors.green, 3),
              width: 50,
              height: 50,
            ),
            Container(
              margin: EdgeInsets.all(5),
              decoration: myCustomDecoration(Colors.blue, 3),
              width: 50,
              height: 50,
            ),
            Container(
              margin: EdgeInsets.all(5),
              decoration: myCustomDecoration(Colors.red, 3),
              width: 50,
              height: 50,
            )
          ],
        ),
        Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.all(5),
                width: 50,
                height: 50,
                decoration: myCustomDecoration(Colors.orange, 3))
          ],
        )
      ]));
  return estructura;
}

myCustomDecoration(Color color, double ancho) {
  return BoxDecoration(
      color: color, border: Border.all(color: Colors.black, width: ancho));
}
