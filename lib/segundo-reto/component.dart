import 'package:flutter/material.dart';

Widget component() {
  var estructura = Scaffold(
      appBar: AppBar(title: Text("Segundo Reto Platzi"), centerTitle: true),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            decoration: myCustomDecoration(Colors.black),
            child: Image.network(
              "https://i.pinimg.com/originals/5d/af/49/5daf49fdf7a84c5eb536b285796029de.jpg",
              fit: BoxFit.cover,
              height: double.infinity,
            ),
          ),
          Container(
            decoration: myCustomDecoration(Colors.black.withOpacity(0.3)),
            width: double.infinity,
            height: 50,
            alignment: Alignment.center,
            child: Text(
              "Cazador en el nuevo mundo",
              style: TextStyle(color: Colors.white, fontSize: 20),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ));
  return estructura;
}

myCustomDecoration(Color color, [Color borderColor, double ancho]) {
  BoxDecoration customBoxDecoration = BoxDecoration(color: color);
  if (borderColor != null && ancho != null) {
    customBoxDecoration = BoxDecoration(
        color: color, border: Border.all(color: borderColor, width: ancho));
  }

  return customBoxDecoration;
}
